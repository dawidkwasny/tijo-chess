package pl.edu.pwsztar.service.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.edu.pwsztar.domain.dto.FigureMoveDto;
import pl.edu.pwsztar.domain.enums.FigureType;
import pl.edu.pwsztar.service.ChessService;

@Service
public class ChessServiceImpl implements ChessService {

    @Autowired
    public ChessServiceImpl() {

    }

    @Override
    public boolean isCorrectMove(FigureMoveDto figureMoveDto) {
        FigureType figureType = figureMoveDto.getType();
        return figureType.checkMove(figureMoveDto.getStart(), figureMoveDto.getDestination());
    }
}
