package pl.edu.pwsztar.domain.enums;

public enum FigureType {
    KING,
    QUEEN,
    ROCK,
    BISHOP,
    KNIGHT,
    PAWN;

    public boolean checkMove(String start, String destination) {
        int startColumn = getColumn(start), startRow = getRow(start),
        destinationColumn = getColumn(destination), destinationRow = getRow(destination);

        switch (this) {
            case BISHOP:
                return (Math.abs(destinationRow-startRow) == Math.abs(destinationColumn-startColumn));
            default:
                throw new AssertionError("Unknown figure " + this);
        }
    }

    private int getColumn(String str) {
        return (int) str.charAt(0) + 1 - 'a';
    }

    private int getRow(String str) {
        return (int) str.charAt(2);
    }
}
